# Problem 1:
* A - 2
* B - 4
* C - 5 // should be strictly increasing
* D - 3
* E - 5 // should be oscillating firstly and then stabilaze on zero
* F - 1

# Problem 2:
* A - 2
* B - 1
* C - 3
* D - 5 // similar to 4, but doesn't goe back to positive after being negative
* E - 4
* F - 5 // similar to 4, but oscillating much faster and longer

# Problem 3:
* a: G(1) = (0.1-0.2)/(1-0.1) = -1/9
* b: G(1) = 0.1 / (1-0.9) = 1
* c: G(1) = -10 * 0.9 * 0.8 / (0.1 * 0.2) = -360
* d: G(1) = 2/ -1 = -2

# Problem 4:
* A - 5
* B - 3
* C - 2
* D - 4
* E - 1
